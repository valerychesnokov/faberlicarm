﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FaberlicARM
{
    /// <summary>
    /// Логика взаимодействия для InputBoxDlg.xaml
    /// </summary>
    public partial class InputBoxDlg : Window
    {
        public string TextValue { get; set; }

        public InputBoxDlg(string text_value)
        {
            InitializeComponent();

            // set Init text value
            this.TextValueBox.Text = text_value;
            TextValue = text_value;
        }

        //public InputBoxDlg(string text_value, int min_val, int max_val)
        //{
        //    InitializeComponent();

        //    // set Init text value
        //    this.TextValue.Text = text_value;
        //    m_TextValue = text_value;

        //    for (int i = min_val; i <= max_val; i++)
        //        this.TextValue.Items.Add(i.ToString());
        //}

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            // return Cancel
            this.DialogResult = false;
            this.Close();
        }

        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            // return OK
            TextValue = this.TextValueBox.Text;
            this.DialogResult = true;
            this.Close();
        }
    }
}
