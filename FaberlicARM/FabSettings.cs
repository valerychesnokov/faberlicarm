﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaberlicARM
{
    public class FabSettings
    {
        public static readonly string[] Names = { "Оля", "Таня", "Катя", "Ира", "Маша", "Саша", "Андрей", "Костя", "Света", "Юля", "Аня", "Семён", "Валя", "Лена", "Паша" };

        public static readonly string MANY_BIG_STATUS_NOT_SUPPORTED = "В текущей версии расчёт ведётся менее статуса Старшего Директора. Данные ОС обнулены.";

        /// <summary>
        /// Имя валюты (руб. для РФ).
        /// </summary>
        public string CurrencyName { get; set; } = "руб.";

        /// <summary>
        /// Сколько рублей (денег валюты) выплачивается при расчётах ОС. 60 руб. для РФ.
        /// </summary>
        public int CurrencyUnitPayoutPrice { get; set; } = 60;

        private static readonly Random getrandom = new Random();

        public static int GetRandomNumber(int min, int max)
        {
            lock (getrandom) // synchronize
            {
                return getrandom.Next(min, max);
            }
        }

        /// <summary>
        /// Return Random Name, exclude sExceptName.
        /// </summary>
        /// <param name="sExceptName"></param>
        /// <returns></returns>
        public static string GetRandomName(string sExceptName)
        {
            string name;

            while (true)
            {
                int index = GetRandomNumber(0, FabSettings.Names.Length);
                name = FabSettings.Names[index];
                if (name != sExceptName)
                    return name;
            }
        }
    }
}
