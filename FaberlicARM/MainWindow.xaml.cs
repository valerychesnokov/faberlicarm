﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace FaberlicARM
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        protected List<FabNode> m_FabNodes = null;

        protected FabSettings m_Settings = new FabSettings();

        public MainWindow()
        {
            InitializeComponent();

            this.Width = SystemParameters.PrimaryScreenWidth * 0.8;
            this.Height = SystemParameters.PrimaryScreenHeight * 0.8;

            // Create start Faberlic Tree config
            FillAddRandomMembers(lo_min: 10, lo_max: 75, members_count: 0);

            // set focus to treeViewCons
            treeViewCons.Focus();

            // select root item
            var obj = treeViewCons.Items[0];
            TreeViewItem tvi = treeViewCons.ItemContainerGenerator.ContainerFromItem(obj) as TreeViewItem;
            if (tvi != null)
            {
                tvi.IsSelected = true;
            }

            //TreeView_ExpandAll(treeViewCons, true);
        }

        protected void TreeStructureClear()
        {
            m_FabNodes = new List<FabNode>();

            Random rand = new Random();
            FabNode root_item_YOU = new FabNode { Name = "Вы", LO = rand.Next(15, 76) };
            m_FabNodes.Add(root_item_YOU);

            treeViewCons.ItemsSource = m_FabNodes;
        }

        /// <summary>
        /// Add Members to Cons Structure.
        /// </summary>
        /// <param name="members_count"></param>
        /// <param name="lo_min"></param>
        /// <param name="lo_max"></param>
        protected void FillAddRandomMembers(int lo_min, int lo_max, int members_count = 0)
        {
            m_FabNodes = new List<FabNode>();

            Random rand = new Random();

            // check & correct members_count
            if (members_count < 1)
                members_count = rand.Next(5, 55);

            FabNode prev_item = null;
            FabNode root_item_YOU = new FabNode { Name = "Вы", LO = rand.Next(lo_min, 3 * lo_max) };

            m_FabNodes.Add(root_item_YOU);

            for (int i = 0; i < members_count; i++)
            {
                // Try find random parent Node
                bool bNextParent = (rand.Next(100) % 9 != 0) && (prev_item != null);

                FabNode item = new FabNode
                {
                    LO = rand.Next(lo_min, lo_max)
                    , Name = FabSettings.GetRandomName(bNextParent ? prev_item.Name : root_item_YOU.Name) 
                };

                if (bNextParent)
                {
                    // add item & change prev_item
                    prev_item = prev_item.AddNode(item);
                }
                else
                {
                    // add item
                    root_item_YOU.AddNode(item);
                    // change prev_item
                    prev_item = root_item_YOU;
                }
            }

            treeViewCons.ItemsSource = m_FabNodes;
        }

        /// <summary>
        /// Expand all Cons nodes.
        /// </summary>
        /// <param name="items"></param>
        /// <param name="expand"></param>
        private void TreeView_ExpandAll(ItemsControl items, bool expand)
        {
            foreach (object obj in items.Items)
            {
                ItemsControl childControl = items.ItemContainerGenerator.ContainerFromItem(obj) as ItemsControl;
                if (childControl != null)
                {
                    TreeView_ExpandAll(childControl, expand);
                }

                TreeViewItem item = childControl as TreeViewItem;

                if (item != null)
                    item.ExpandSubtree();
            }
        }

        /// <summary>
        /// Add new Child Cons node.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FabItemAddChild_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FabNode parent_item = treeViewCons.SelectedItem as FabNode;
                if (parent_item == null)
                {
                    MessageBox.Show("Выберите консультанта", "Сообщение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                treeViewCons.ItemsSource = null;

                Random rand = new Random();

                FabNode new_item = new FabNode 
                {
                    LO = rand.Next(0, 55)
                    , Name = FabSettings.GetRandomName(parent_item.Name)
                };

                parent_item.AddNode(new_item);

                // After change structure's nodes
                m_FabNodes[0].ResetOsFields(m_Settings);

                treeViewCons.ItemsSource = m_FabNodes;

                treeViewCons.Focus();
            }
            catch (Exception ex)
            {
                ShowError(ex);
            }
        }

        /// <summary>
        /// Show and save Exception
        /// </summary>
        private void ShowError(Exception ex)
        {
            MessageBox.Show("ОШИБКА: " + ex.Message, "ОШИБКА", MessageBoxButton.OK, MessageBoxImage.Error);

            // Save to Log
            // !!!!!!!!!!
        }

        /// <summary>
        /// Click to FaberLand Hyperlink.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FaberLandHyperlink_Click(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        /// <summary>
        /// Delete current Cons.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FabItemDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Удалить выбранного консультанта\r\nсо ВСЕЙ его структурой?", "Сообщение", MessageBoxButton.OKCancel, MessageBoxImage.Question)
                    != MessageBoxResult.OK)
                    return;

                FabNode removed_item = treeViewCons.SelectedItem as FabNode;
                if (removed_item == null)
                {
                    MessageBox.Show("Выберите консультанта", "Сообщение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                treeViewCons.ItemsSource = null;

                bool bRet = FabNode.DeleteNode(removed_item);

                if (!bRet)
                {
                    MessageBox.Show("Нельзя удалить СЕБЯ! 😃\r\nСтр-а-а-а-а-ш-но:)))", "Сообщение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                else
                {
                    // After change structure's nodes
                    m_FabNodes[0].ResetOsFields(m_Settings);
                }

                treeViewCons.ItemsSource = m_FabNodes;
            }
            catch (Exception ex) {
                ShowError(ex);
            }
        }

        /// <summary>
        /// Calc of the Marketing Plan SUM.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FabItemCalcMPlan_Click(object sender, RoutedEventArgs e)
        {
            CalcMPlan();
        }

        /// <summary>
        /// Move Cons item (node).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FabItemMove_Click(object sender, RoutedEventArgs e)
        {
            // !!!!!!!!!!
            MessageBox.Show("В разработке", "Сообщение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void FabItemSettings_Click(object sender, RoutedEventArgs e)
        {
            // !!!!!!!!!!
            MessageBox.Show("В разработке", "Сообщение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void FabItemFillStructure_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Заполнить структуру заново случайным образом?"
                    , "Сообщение", MessageBoxButton.OKCancel, MessageBoxImage.Question)
                    != MessageBoxResult.OK)
                    return;

                // Fill
                FillAddRandomMembers(lo_min: 20, lo_max: 75, members_count: 0);
            }
            catch (Exception ex)
            {
                ShowError(ex);
            }
        }

        private void FabItemClearStructure_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Очистить структуру?"
                    , "Сообщение", MessageBoxButton.OKCancel, MessageBoxImage.Question)
                    != MessageBoxResult.OK)
                    return;

                // Clear
                TreeStructureClear();
            }
            catch (Exception ex)
            {
                ShowError(ex);
            }
        }

        /// <summary>
        /// Calc Marketing Plan Faberlic for all structure (fill OS_SUM fields).
        /// </summary>
        private void CalcMPlan()
        {
            try {
                treeViewCons.ItemsSource = null;

                //m_FabNodes[0].Settings = m_Settings;
                m_FabNodes[0].CalcOsSum(m_Settings);

                treeViewCons.ItemsSource = m_FabNodes;

                // Save Structure Data to local file
                //string file_name_xml = "fabdata_xml.txt";
                //FabStorage.SaveDataToLocalFile(file_name_xml, m_FabNodes);

                if (String.IsNullOrEmpty(m_FabNodes[0].MessageOfCalcLog))
                    MessageBox.Show("Расчёт ОС произведён, данные записаны"
                        , "Сообщение", MessageBoxButton.OK, MessageBoxImage.Information);
                else
                    MessageBox.Show("Расчёт ОС произведён, итоги: " + m_FabNodes[0].MessageOfCalcLog
                        , "Сообщение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            catch (Exception ex)
            {
                ShowError(ex);
            }
        }

        private void FabItemEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FabNode parent_item = treeViewCons.SelectedItem as FabNode;
                if (parent_item == null)
                {
                    MessageBox.Show("Выберите консультанта", "Сообщение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                //string var = new InputBox(parent_item.LO.ToString(), "Правка данных для: " + parent_item.Name, parent_item.LO.ToString()).ShowDialog();

                string sLO_val = parent_item.LO.ToString();

                InputBoxDlg dlg = new InputBoxDlg(text_value: sLO_val); ///, min_val: 0, max_val: 100);
                dlg.ShowDialog();

                bool result = (bool)dlg.DialogResult;

                if (result)
                { // if dialog return OK
                    parent_item.LO = Int32.Parse(dlg.TextValue);

                    treeViewCons.ItemsSource = null;

                    // After change structure's nodes
                    m_FabNodes[0].ResetOsFields(m_Settings);

                    treeViewCons.ItemsSource = m_FabNodes;
                }

                treeViewCons.Focus();
            }
            catch (Exception ex)
            {
                ShowError(ex);
            }
        }

        private void FabStorageSave_Click(object sender, RoutedEventArgs e)
        {
            // Save Structure Data to local file
            string file_name_xml = "fabdata_xml.txt";
            
            // select file name & path
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            // Save Structure
            FabStorage.SaveDataToLocalFile(file_name_xml, m_FabNodes);

            MessageBox.Show("Структура сохранена в файл: " + file_name_xml
                , "Сообщение", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void FabStorageLoad_Click(object sender, RoutedEventArgs e)
        {
            // Load Structure Data from local file
            string file_name_xml = "fabdata_xml.txt";

            // select file name & path
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            // Save Structure
            FabStorage.LoadDataFromLocalFile(file_name_xml, ref m_FabNodes);

            MessageBox.Show("Структура прочитана из файла: " + file_name_xml
                , "Сообщение", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
