﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
//using Newtonsoft.Json;

namespace FaberlicARM
{
    public class FabStorage
    {
        //public static void SaveDataToLocalFile(string file_name, Object obj)
        //{
        //    try
        //    {
        //        string jsonStr = JsonConvert.SerializeObject(obj);

        //        using (StreamWriter outputFile = new StreamWriter(file_name))
        //        {
        //            outputFile.Write(jsonStr);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        //public static void SaveData2(string file_name, List<FabNode> nodes)
        //{
        //    try
        //    {
        //        XmlSerializer ser = new XmlSerializer(typeof(FabNode));
        //        using (FileStream fs = new FileStream(file_name, FileMode.Create))
        //        {
        //            ser.Serialize(fs, nodes);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        //string s = ex.Message;
        //        //Exception inex = ex.InnerException;
        //        throw;
        //    }
        //}

        public static void SaveDataToLocalFile(string filename, List<FabNode> nodes)
        {
            StreamWriter stream = null;

            try
            {
                XmlSerializer mySerializer = new XmlSerializer(typeof(List<FabNode>));
                stream = new StreamWriter(filename);

                mySerializer.Serialize(stream, nodes);
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                Exception inex = ex.InnerException;
                throw;
            }
            finally
            {
                if (stream != null)
                stream.Close();
            }
        }

        public static void LoadDataFromLocalFile(string filename, ref List<FabNode> nodes)
        {
            StreamReader stream = null;

            try
            {
                XmlSerializer mySerializer = new XmlSerializer(typeof(List<FabNode>));
                stream = new StreamReader(filename);

                nodes = (List<FabNode>)mySerializer.Deserialize(stream);
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                Exception inex = ex.InnerException;
                throw;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
        }
    }
}
